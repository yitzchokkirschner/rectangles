const Rectangles = require('./rectangel');

const rect1 = new Rectangles(10, 10)
const rect2 = new Rectangles(12, 10)

test('Checking the perimeter for a 10 over 10 square', () => {
    expect(rect1.perimeter).toBe(40);
});

test('Checking the area of a 12 by 10 rectangle', () => {
    expect(rect2.area).toBe(120);
});

test('Checking the outcome of two equel numbers is true', () => {
    expect(rect1.isItSquare).toBe(true);
});

test('Checking the outcome of two non - equel numbers is false', () => {
    expect(rect2.isItSquare).toBe(false);
});