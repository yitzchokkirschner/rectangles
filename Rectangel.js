module.exports = class Rectangles {
    constructor(height, width) {
        this.height = height;
        this.width = width;
    }
    //Getter
    get perimeter() {
        return this.calcPerimeter();
    }

    //Getter
    get area() {
        return this.calcArea();
    }

    //Getter
    get isItSquare() {
        return this.isSquare();
    }

    calcPerimeter() {
        return (this.height * 2) + (this.width *2);
    }

    calcArea() {
        return (this.height * this.width);
    }

    isSquare() {
        if (this.height === this.width) {
            return true;
        return false;
        } 
        
    }
}
  